package com.asm;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
public class Worker
{
	public static void main(String[] args) throws Exception
	{
		Class.forName("com.mysql.jdbc.Driver");
		Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/Worker_Managment","root","password");
		Statement stmt = con.createStatement();
		ResultSet rs=stmt.executeQuery("SELECT concat(first_name , last_name) AS FullName FROM Worker_table;");
		System.out.println(" --------------" );
		System.out.println("|  Full Name  |" );
		System.out.println(" --------------" );
		while(rs.next())
		{
			System.out.println("  "+rs.getString(1));
		}
		System.out.println("\n\n");
		ResultSet rs1=stmt.executeQuery("SELECT Distinct department FROM Worker_table;");
		System.out.println(" ----------------------" );
		System.out.println("| Distinct Departments |" );
		System.out.println(" ----------------------" );
		while(rs1.next())
		{
			System.out.println("  "+rs1.getString(1));
		}
		ResultSet rs2=stmt.executeQuery("SELECT first_name, POSITION('a' IN first_name)FROM Worker_table");
		System.out.println(" ------------------------------" );
		System.out.println("| FirstName \tPosition of a  |" );
		System.out.println(" ------------------------------" );
		while(rs2.next())
		{
			System.out.println("  "+rs2.getString(1)+"\t"+rs2.getInt(2));
		}
	}
}
